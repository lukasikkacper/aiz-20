package me.kacperlukasik;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Main
{

    public static void zad2()
    {
        Scanner sc = new Scanner(System.in);


        System.out.println("Zad 2)");
        Stos zad2 = new Stos();
        System.out.println("Podaj wyrazenie:");
        String zad2Line = sc.nextLine();
        for (int i = 0; i < zad2Line.length(); i++)
        {
            char x = zad2Line.charAt(i);

            if (x == ' ')
                continue;
            if (x == '(')
                zad2.push(x);
            if (x == ')')
            {
                try
                {
                    zad2.pop();
                } catch (Exception e)
                {
                    System.out.println("Wyrażenie nie jest prawidłowe !");
                    return;
                }
            }
        }

        if (zad2.isEmpty())
            System.out.println("Wyrażenie prawidłowe !");
        else
            System.out.println("Wyrażenie nie jest prawidłowe !");
    }

    public static void zad3()
    {
        Scanner sc = new Scanner(System.in);
        Stos stos = new Stos();
        System.out.println("Zad3)\nPodaj słowo:");
        String x = sc.nextLine();
        StringBuilder tmp = new StringBuilder();

        for (int i = 0; i < x.length(); i++)
        {
                stos.push(x.charAt(i));
        }

        try
        {
            for (int i = 0; i < x.length(); i++)
                tmp.append((char) stos.pop());
        } catch (Exception e)
        {
            System.out.println("Słowo nie jest palindromem");
            return;
        }

        if (x.equalsIgnoreCase(tmp.toString()))
            System.out.println("Słowo jest palindromem");
        else
            System.out.println("Słowo nie jest palindromem");
    }

    public static void zad4()
    {
        Scanner sc = new Scanner(System.in);
        Stos stos = new Stos();
        System.out.println("Zad4)\nWpisz wyrazenie ONP:");
        String x = sc.nextLine();
        StringTokenizer tokenizer = new StringTokenizer(x, " ");
        while (tokenizer.hasMoreTokens())
        {
            String value = tokenizer.nextElement().toString();

            if (!"+ - / * ^".contains(value))
                stos.push(Integer.parseInt(value));
            else
            {
                try
                {
                    int b = stos.pop();
                    int a = stos.pop();

                    switch (value)
                    {
                        case "+":
                            stos.push(a + b);
                            break;
                        case "-":
                            stos.push(a - b);
                            break;
                        case "*":
                            stos.push(a * b);
                            break;
                        case "/":
                            stos.push(a / b);
                            break;
                        case "^":
                            stos.push((int) Math.pow(a,b));
                            break;
                    }

                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        try
        {
            System.out.printf("Wynik = %d\n",stos.peek());
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        zad2();
        zad2();
        zad3();
        zad3();
        zad4();
    }
}
