package me.kacperlukasik;

public class Stos implements IStos
{
    private Element top;

    public Stos()
    {
        this.top = null;
    }

    @Override
    public void push(int i)
    {
        Element x = new Element();

        x.dane = i;
        x.next = top;
        this.top = x;
    }

    @Override
    public int pop() throws Exception
    {
        if (isEmpty())
            throw new Exception("Stos pusty");

        int x = top.dane;
        top = top.next;

        return x;
    }

    @Override
    public int peek() throws Exception
    {
        if (isEmpty())
            throw new Exception("Stos pusty");

        return top.dane;
    }

    @Override
    public boolean isEmpty()
    {
        return  top == null;
    }

    @Override
    public void print() throws Exception
    {
        if (isEmpty())
            throw new Exception("Stos pusty");

        Element x = top;

        while (x!=null)
        {
            System.out.printf("%d->", x.dane);
            x = x.next;
        }
    }

    @Override
    public void clear()
    {
        top = null;
    }
}
