package me.kacperlukasik;

public interface IStos
{
    void push(int i) throws Exception;
    int pop() throws Exception;
    int peek() throws Exception;
    boolean isEmpty();
    void print() throws Exception;
    void clear();
}
