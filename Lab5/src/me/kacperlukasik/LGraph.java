package me.kacperlukasik;

import java.util.ArrayList;

public class LGraph extends AGraph
{
    private ArrayList<ArrayList<Integer>> graph = new ArrayList<>();

    public LGraph(int vertexCount)
    {
        super(vertexCount);
        setGraphDefault();
    }

    @Override
    public void writeMatrix()
    {
        System.out.print("|W|");
        for (int i = 0; i < graph.size(); i++)
            System.out.format("|%d|", i);
        System.out.println();
        for (int i = 0; i < graph.size(); i++)
        {
            System.out.format("|%d|", i);
            for (int j = 0; j < graph.size(); j++)
                System.out.format("|%d|", check(i,j) ? 1 : 0);
            System.out.println();
        }
    }

    @Override
    public boolean check(int i, int j) throws IllegalArgumentException
    {
        if (i < 0 || j < 0)
            throw new IllegalArgumentException("I oraz J nie może być mniejsze od zera");
        if (i > size -1 || j > size - 1)
            throw new IllegalArgumentException("I oraz J nie może być większe od rozmiaru grafu");

        return graph.get(i).contains(j);
    }

    @Override
    public void connect(int i, int j) throws IllegalArgumentException
    {
        if (i < 0 || j < 0)
            throw new IllegalArgumentException("I oraz J nie może być mniejsze od zera");
        if (i > size -1 || j > size - 1)
            throw new IllegalArgumentException("I oraz J nie może być większe od rozmiaru grafu");

        if (!graph.get(i).contains(j))
            graph.get(i).add(j);
    }

    @Override
    public void writeList()
    {
        for (int i = 0; i < graph.size(); i++)
        {
            System.out.print(i+": ");

            for (int j = 0; j < graph.size(); j++)
            {
                if (check(i,j))
                {
                    System.out.format(" %d ", j);
                }
            }
            System.out.println();
        }
    }

    @Override
    public void writeNoNeighbor()
    {
        for (int i = 0; i < graph.size(); i++)
        {
            if (graph.get(i).isEmpty())
                System.out.format("Wierzchołek bez sąsiada -> %d\n", i);
        }
    }

    @Override
    public void transposition()
    {
        LGraph newGraph = new LGraph(size);


        for (int i = 0; i < graph.size(); i++)
        {
            for (int j = 0; j < graph.size(); j++)
            {
                if (check(i,j))
                    newGraph.connect(j,i);
            }
        }

        graph = newGraph.graph;
    }

    private void setGraphDefault()
    {
        for(int i = 0; i < size; i++)
        {
            graph.add(new ArrayList<Integer>());
        }
    }
}
