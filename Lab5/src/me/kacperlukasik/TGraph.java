package me.kacperlukasik;

public class TGraph extends AGraph
{
    private int[][] graph;

    public TGraph(int vertexCount)
    {
        super(vertexCount);
        setGraphDefault();
    }

    @Override
    public void writeMatrix()
    {
        System.out.print("|W|");
        for (int i = 0; i < graph.length; i++)
            System.out.format("|%d|", i);
        System.out.println();
        for (int i = 0; i < graph.length; i++)
        {
            System.out.format("|%d|", i);
            for (int j = 0; j < graph.length; j++)
                System.out.format("|%d|", graph[i][j]);
            System.out.println();
        }
    }

    @Override
    public boolean check(int i, int j) throws IllegalArgumentException
    {
        if (i < 0 || j < 0)
            throw new IllegalArgumentException("I oraz J nie może być mniejsze od zera");
        if (i > size -1 || j > size - 1)
            throw new IllegalArgumentException("I oraz J nie może być większe od rozmiaru grafu");

        return graph[i][j] == 1 ? true : false;
    }

    @Override
    public void connect(int i, int j) throws IllegalArgumentException
    {
        if (i < 0 || j < 0)
            throw new IllegalArgumentException("I oraz J nie może być mniejsze od zera");
        if (i > size -1 || j > size - 1)
            throw new IllegalArgumentException("I oraz J nie może być większe od rozmiaru grafu");

        graph[i][j] = 1;
    }

    @Override
    public void writeList()
    {
        for (int i = 0; i < graph.length; i++)
        {
            System.out.print(i+": ");

            for (int j = 0; j < graph.length; j++)
            {
                if (check(i,j))
                {
                    System.out.format(" %d ", j);
                }
            }
            System.out.println();
        }
    }

    @Override
    public void writeNoNeighbor()
    {
        for (int i = 0; i < graph.length; i++)
        {
            if (checkInRow(i))
            {
                System.out.format("Wierzchołek bez sąsiada -> %d\n", i);
            }
        }
    }

    @Override
    public void transposition()
    {
        TGraph newGraph = new TGraph(size);


        for (int i = 0; i < graph.length; i++)
        {
            for (int j = 0; j < graph.length; j++)
            {
                if (check(i,j))
                    newGraph.connect(j,i);
            }
        }

        graph = newGraph.graph;
    }

    private void setGraphDefault()
    {
        graph = new int[size][size];

        for (int i = 0; i < graph.length; i++)
            for (int j = 0; j < graph.length; j++)
                graph[i][j] = 0;
    }

    private boolean checkInRow(int i)
    {
        for(int j = 0; j < size; j++)
        {
            if (check(i,j))
                return false;
        }

        return true;
    }
}
