package me.kacperlukasik;

public class Main {

    public static void main(String[] args) {
        System.out.println("=========================================");
        System.out.println("GRAF WYKORZYSTUJĄCY MACIERZ SĄSIEDZTWA");
        System.out.println("=========================================");
        TGraph tGraph = new TGraph(3);
        tGraph.connect(0,1);
        tGraph.connect(1,2);
        System.out.println("MATRIX:");
        tGraph.writeMatrix();
        System.out.println("LISTA:");
        tGraph.writeList();
        System.out.println("BEZ SASIADOW:");
        tGraph.writeNoNeighbor();
        System.out.println("Graf skierowany ? - >" + tGraph.isDirected() );
        tGraph.connect(1,0);
        tGraph.connect(1,2);
        tGraph.connect(2,1);
        System.out.println("Graf skierowany ? -> " + tGraph.isDirected() );
        System.out.println("Graf jest kliką ? -> " + tGraph.isClique());
        tGraph.connect(0,2);
        tGraph.connect(2,0);
        tGraph.writeMatrix();
        System.out.println("Graf jest kliką ? -> " + tGraph.isClique());
        System.out.println("Transpozycja grafu ->");
        tGraph.transposition();
        tGraph.writeMatrix();

        System.out.println("=========================================");
        System.out.println("GRAF WYKORZYSTUJĄCY LISTĘ LIST SĄSIEDZTWA");
        System.out.println("=========================================");

        LGraph lGraph = new LGraph(3);
        lGraph.connect(0,1);
        lGraph.connect(1,2);
        System.out.println("MATRIX:");
        lGraph.writeMatrix();
        System.out.println("LISTA:");
        lGraph.writeList();
        System.out.println("BEZ SASIADOW:");
        lGraph.writeNoNeighbor();
        System.out.println("Graf skierowany ? - >" + lGraph.isDirected() );
        lGraph.connect(1,0);
        lGraph.connect(1,2);
        lGraph.connect(2,1);
        System.out.println("Graf skierowany ? -> " + lGraph.isDirected() );
        System.out.println("Graf jest kliką ? -> " + lGraph.isClique());
        lGraph.connect(0,2);
        lGraph.connect(2,0);
        System.out.println("Graf jest kliką ? -> " + lGraph.isClique());
        System.out.println("Transpozycja grafu ->");
        lGraph.transposition();
        lGraph.writeMatrix();
    }
}
