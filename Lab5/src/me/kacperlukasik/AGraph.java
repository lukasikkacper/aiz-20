package me.kacperlukasik;

/**
 * Klasa abstrakcyjna reprezentujaca graf.
 */
public abstract class AGraph
{

    protected int size;

    public AGraph(int vertexCount)
    {
        size = vertexCount;
        if (size <= 0)
        {
            throw new IllegalArgumentException("Rozmiar grafu musi byc wiekszy od zera!");
        }
    }

    public int getSize()
    {
        return size;
    }

    /**
     * Wypisuje macierz sasiedztwa grafu.
     */
    public abstract void writeMatrix();

    /**
     * Sprawdza, czy istnieje krawedz pomiedzy wierzcholkiem i oraz j.
     */
    public abstract boolean check(int i, int j) throws IllegalArgumentException;

    /**
     * Tworzy krawedz pomiedzy wierzcholkiem i oraz j.
     */
    public abstract void connect(int i, int j) throws IllegalArgumentException;

    /**
     * Wypisuje graf jako listy sasiedztwa.
     */
    public abstract void writeList();

    /**
     * Wypisuje wierzchołki bez sąsiada.
     */
    public abstract void writeNoNeighbor();

    /**
     * Sprawdza, czy graf jest nieskierowany.
     */
    public boolean isDirected()
    {
        for (int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
            {
                if (check(i,j) && !check(j,i))
                    return false;
            }
        }

        return true;
    }

    /**
     * Przeprowadza tranzpozycje grafu.
     */
    public abstract void transposition();

    /**
     * Sprawdza, czy graf jest kliką.
     */
    public boolean isClique()
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                if (i != j)
                {
                    if (!check(i, j))
                        return false;
                }
            }
        }

        return true;
    }
}

