package me.kacperlukasik;

import java.util.List;

public class Main
{

    public static void main(String[] args) throws Exception
    {
        WGraph g = new WGraph(4);
        WGraph wGraph = new WGraph();

        wGraph.addVertices(5);
        wGraph.addEdge(0, 1, 10);
        wGraph.addEdge(0, 4, 5);
        wGraph.addEdge(1, 2, 1);
        wGraph.addEdge(1, 4, 2);
        wGraph.addEdge(2, 3, 4);
        wGraph.addEdge(3, 2, 6);
        wGraph.addEdge(3, 0, 7);
        wGraph.addEdge(4, 2, 9);
        wGraph.addEdge(4, 1, 3);
        wGraph.addEdge(4, 3, 2);
        System.out.println("Algorytm forda:");
        wGraph.ford(0);
        System.out.println("Algorytm dijkstra:");
        wGraph.dijkstra(0);
        System.out.println("SSS:");
        g.addEdge(0, 1, 1);
        g.addEdge(0, 3, 1);
        g.addEdge(1, 2, 1);
        g.addEdge(2, 0, 1);
        g.writeGraph();
        g.SSS();
    }
}
