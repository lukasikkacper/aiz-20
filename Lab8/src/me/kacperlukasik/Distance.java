package me.kacperlukasik;

public class Distance implements Comparable<Distance>
{
    private int index;
    private int weight;
    private boolean isShorted;

    public Distance(int index, int weight)
    {
        this.index = index;
        this.weight = weight;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public int getWeight()
    {
        return weight;
    }

    public void setWeight(int weight)
    {
        this.weight = weight;
    }

    public boolean isShorted()
    {
        return isShorted;
    }

    public void setShorted(boolean shorted)
    {
        isShorted = shorted;
    }

    @Override
    public int compareTo(Distance o)
    {
        return Integer.compare(this.weight,o.weight);
    }
}
