package me.kacperlukasik;

import java.lang.reflect.Array;
import java.util.*;

public class WGraph implements IWGraph
{
    private int[][] wGraph;
    private int id;

    public WGraph()
    {
        wGraph = new int[0][0];
    }

    public WGraph(int n)
    {
        wGraph = new int[n][n];
    }

    @Override
    public void addVertices(int i) throws Exception
    {
        if (i < 0)
            throw new Exception("Podano ujemną ilość wierzchołków");

        int[][] tempGraph = new int[i + wGraph.length][i + wGraph.length];

        for (int x = 0; x < tempGraph.length; x++)
        {
            Arrays.fill(tempGraph[x], IWGraph.INFTY);
        }

        for (int x = 0; x < wGraph.length; x++)
        {
            System.arraycopy(wGraph[x], 0, tempGraph[x], 0, wGraph[x].length);
        }

        wGraph = tempGraph;
    }

    @Override
    public void addEdge(int start, int stop, int weight) throws Exception
    {
        if (start > wGraph.length - 1 || stop > wGraph.length - 1 || start < 0 || stop < 0)
            throw new Exception("Wierzchołek żródłowy lub docelowy nie istnieje");
        if (weight < 0)
            throw new Exception("Waga jest ujemna !");

        wGraph[start][stop] = weight;
    }

    @Override
    public int wCheck(int start, int stop) throws Exception
    {
        if (start > wGraph.length - 1 || stop > wGraph.length - 1 || start < 0 || stop < 0)
            throw new Exception("Wierzchołek żródłowy lub docelowy nie istnieje");

        return wGraph[start][stop];
    }

    @Override
    public void writeGraph() throws Exception
    {
        System.out.print("|  W |");
        for (int i = 0; i < wGraph.length; i++)
            System.out.format("|  %d |", i);
        System.out.println();
        for (int i = 0; i < wGraph.length; i++)
        {
            System.out.format("|  %d |", i);
            printDistanceRow(wGraph[i]);
        }
    }

    @Override
    public void ford(int start) throws Exception
    {
        int q = wGraph.length;
        int[] distance = new int[q];

        for (int i = 0; i < q; i++) distance[i] = wGraph[start][i];

        for (int k = 1; k < q - 2; k++)
        {
            for (int v = 1; v < q; v++)
            {
                for (int u = 1; u < q; u++)
                {
                    distance[v] = Math.min(distance[v], distance[u] + wGraph[u][v]);
                }
            }
        }


        for (int x = 1; x < q; x++)
        {
            if (distance[x] == IWGraph.INFTY)
                System.out.format("Brak drogi z %d do %d\n", start, x);
            else
                System.out.format("Najkrótszaz droga z %d do %d wynosi: %d\n", start, x, distance[x]);
        }
    }

    @Override
    public void dijkstra(int start) throws Exception
    {
        int n = wGraph.length;
        int distance[] = new int[n];
        boolean[] isShorted = new boolean[n];
        List<Distance> q = new ArrayList<>();

        for (int i = 0; i < n; i++)
        {
            Distance dis = new Distance(i, i == 0 ? 0 : wGraph[start][i]);
            distance[i] = dis.getWeight();
            q.add(dis);
        }

        Distance acc = Collections.min(q);
        q.remove(acc);

        printHeaders();

        while (!q.isEmpty())
        {
            for (int i = 0; i < n; i++)
            {
                if (distance[i] > distance[acc.getIndex()] + wGraph[acc.getIndex()][i])
                {
                    distance[i] = distance[acc.getIndex()] + wGraph[acc.getIndex()][i];
                }
            }

            printDistanceRow(distance);
            isShorted[acc.getIndex()] = true;
            acc = Collections.min(q);
            q.remove(acc);
        }

        for (int x = 1; x < n; x++)
        {
            if (distance[x] == IWGraph.INFTY)
                System.out.format("Brak drogi z %d do %d\n", start, x);
            else
                System.out.format("Najkrótszaz droga z %d do %d wynosi: %d\n", start, x, distance[x]);
        }
    }

    private void printDistanceRow(int[] distance)
    {
        for (int i = 0; i < distance.length; i++)
        {
            String x = distance[i] == INFTY ? " *" : (String.valueOf(distance[i]).length() == 1 ? " " + String.valueOf(distance[i]) : String.valueOf(distance[i]));

            System.out.format("| %s |", x);
        }
        System.out.println();
    }

    private void printHeaders()
    {
        for (int i = 0; i < wGraph.length; i++)
        {
            System.out.format("|D(%d)|", i);
        }
        System.out.println();
    }

    private void dfs(int w, boolean[] visited)
    {
        visited[w] = true;

        System.out.print(w + " ");

        for (int i = 0; i < wGraph[w].length; i++)
        {
            if (wGraph[w][i] == 0)
                continue;

            if (!visited[i])
                dfs(i, visited);
        }
    }

    private void dfs(int w, boolean[] visited, Stack<Integer> stack)
    {
        visited[w] = true;

        for (int i = 0; i < wGraph[w].length; i++)
        {
            if (wGraph[w][i] == 0)
                continue;

            if (!visited[i])
                dfs(i, visited, stack);
        }

        stack.push(w);
    }

    public WGraph transpose() throws Exception
    {
        int n = wGraph.length;

        WGraph newGraph = new WGraph(n);

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (wGraph[i][j] != 0)
                    newGraph.addEdge(j, i, 1);
            }
        }

        return newGraph;
    }


    @Override
    public void SSS() throws Exception
    {
        int n = wGraph.length;

        Stack<Integer> stack = new Stack<>();
        boolean[] visited = new boolean[n];
        WGraph trans = transpose();

        for (int i = 0; i < n; i++)
        {
            if (!visited[i])
                dfs(i, visited, stack);
        }

        Arrays.fill(visited, false);

        while (!stack.empty())
        {
            int w = stack.pop();

            if (!visited[w])
            {
                trans.dfs(w, visited);
                System.out.println();
            }
        }

    }
}
