package me.kacperlukasik;

public class Main
{

    public static void main(String[] args)
    {
        String x = "ABCABAABCABAC";
        String y = "CAB";

        System.out.println("abababba jest łatwy ? " + SearchPattern.isSimple("abababba"));
        System.out.println("abbabbbab jest łatwy ? " + SearchPattern.isSimple("abbabbbab"));

        System.out.println("==KMP==");
        SearchPattern.KMP(x, y);
        System.out.println("====");

        System.out.println("==GS==");
        SearchPattern.GS(x, y);
        System.out.println("====");

        System.out.println("==BM==");
        SearchPattern.BM(x,y);
        System.out.println("====");
    }

}
