package me.kacperlukasik;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchPattern
{
    public static int searchPattern(String y, String x)
    {
        int counter = 0;
        int n = y.length();
        int m = x.length();

        int i = 0;

        while (i <= n - m)
        {
            int j = 0;

            while (j < m && x.charAt(j) == y.charAt(i + j))
            {
                j++;
            }

            if (j == m)
            {
                counter++;
            }

            i++;
        }

        return counter;
    }

    public static void KMP(String x, String y)
    {
        int count = 0;
        char[] chars = y.toCharArray();
        int[] next = new int[y.length() + 1];

        for (int i = 1; i < y.length(); i++)
        {
            int j = next[i + 1];

            while (j > 0 && chars[j] != chars[i])
                j = next[j];

            if (j > 0 || chars[j] == chars[i])
                next[i + 1] = j + 1;

            j++;
        }

        for (int i = 0, j = 0; i < x.length(); i++)
        {
            if (j < y.length() && x.charAt(i) == y.charAt(j))
            {
                if (++j == y.length())
                {
                    count++;
                    System.out.format("Znaleziona pod index: %d\n", (i - j + 1));
                }
            } else if (j > 0)
            {
                j = next[j];
                i--;
            }
        }

        System.out.println("Ilość wystąpień: " + count);

    }

    public static void GS(String x, String y)
    {
        if (!isSimple(y))
        {
            System.out.println("Podany wrzorzec nie jest łatwy !");

            return;
        }

        int count = 0;
        int i = 0;

        while (i < x.length() - y.length())
        {
            int j = 0;

            while (j < y.length() && (x.charAt(i + j) == y.charAt(j)))
                j++;

            if (j == y.length())
            {
                System.out.format("Znaleziona pod index: %d\n", i);
                count++;
            }

            i += Math.max(1, j / 3);
        }

        System.out.println("Ilość wystąpień: " + count);
    }

    public static void BM(String x, String y)
    {
        int[] last = new int[y.length()];
        int i = 0;
        int count = 0;

        Arrays.fill(last,-1);
        for (int k = 0; k < y.length(); k++)
        {
            last[(int)y.charAt(i) - 65] = i;
        }

        while (i <= (x.length() - y.length()))
        {
            int j = y.length() - 1;

            while (j >= 0 && y.charAt(j) == x.charAt(i+j))
                j--;

            if (j < 0)
            {
                System.out.format("Znaleziona pod index: %d\n", i);
                count++;
            }

            i+=Math.max(1,j-last[(int)x.charAt(i+j) - 65]);

        }

    }

    public static boolean isSimple(String pattern)
    {
        int n = pattern.length();
        int x = n / 3;

        String word = "";

        for (int i = 0; i < x; i++)
        {
            word += pattern.charAt(i);

            for (int j = 0, k = 0; j < 3; j++)
            {
                String word2 = "";

                for (int z = 0; z < word.length() * 3; z++)
                {
                    word2 += pattern.charAt(z);
                }

                if (searchPattern(word2, word) == 3)
                    return false;

                k += word.length();
            }
        }


        return true;
    }



}
