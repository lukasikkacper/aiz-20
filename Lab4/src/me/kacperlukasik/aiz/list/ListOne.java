package me.kacperlukasik.aiz.list;

public class ListOne<T> implements IList<T>, Cloneable
{
    private ElemOne<T> head;

    public ListOne()
    {
        this.head = null;
    }

    @Override
    public void addFirst(T newData)
    {
        if (head == null)
        {
            head = new ElemOne<>(newData);
            return;
        }

        ElemOne temp = new ElemOne(newData);
        temp.setNext(head);
        head = temp;
    }

    @Override
    public void addLast(T newData)
    {
        if (head == null)
        {
            head = new ElemOne<T>(newData);
            return;
        }

        ElemOne<T> current = head;

        while (current.getNext() != null)
        {
            current = current.getNext();
        }

        current.setNext(new ElemOne<T>(newData));
    }

    @Override
    public void addAtPosition(T newData, int position) throws ListException
    {
        ElemOne temp = new ElemOne(newData);

        if (size()<(position-1))
            throw new ListException("Nie ma takiego zakresu w liscie");

        if (head == null && position != 0)
            throw new ListException("Nie ma takiego zakresu w liscie");
        else if (head == null && position == 0)
        {
            head = temp;
            return;
        } else if (position == 0)
        {
            temp.setNext(head);
            head = temp;
            return;
        }

        position--;
        int counter = 0;
        ElemOne current = head;

        while (current.getNext() != null && counter < position)
        {
            if (counter == position)
                break;

            current = current.getNext();
            counter++;
        }

        if (counter == position)
        {
            temp.setNext(current.getNext());
            current.setNext(temp);
        }

    }

    @Override
    public int size()
    {
        if (head==null)
            return 0;

        int size = 1;
        ElemOne current = head;

        while (current.getNext() != null)
        {
            current = current.getNext();
            size++;
        }

        return size;
    }

    @Override
    public T get(int position) throws ListException
    {
        if (head == null)
            throw new ListException("Nie ma takiego zakresu w liscie");
        if (size()<(position-1))
            throw new ListException("Nie ma takiego zakresu w liscie");

        int counter = 0;
        ElemOne current = head;

        while (current.getNext() != null && counter < position)
        {
            current = current.getNext();
            counter++;

            if (counter == position)
                break;
        }

        return (T) current.getData();
    }

    @Override
    public T removeFirst() throws ListException
    {
        if (head == null)
            throw new ListException("Nie ma takiego zakresu w liscie");

        ElemOne temp = head;
        head = head.getNext();

        return (T) temp.getData();
    }

    @Override
    public T removeLast() throws ListException
    {
        if (head == null)
            throw new ListException("Nie ma takiego zakresu w liscie");

        if (head.getNext() == null)
        {
            ElemOne temp = head;
            head = null;
            return (T) temp.getData();
        }

        ElemOne current = head;

        while (current.getNext().getNext() != null)
        {
            current = current.getNext();
        }

        ElemOne temp = current.getNext();
        current.setNext(null);

        return (T) temp.getData();
    }

    @Override
    public T remove(int position) throws ListException
    {
        if (head == null)
            throw new ListException("Nie ma takiego zakresu w liscie");
        if (position < 0 || position > size())
            throw new ListException("Nie ma takiego zakresu w liscie");
        if (position==0)
        {
            ElemOne temp = head;
            head = head.getNext();

            return (T) temp.getData();
        }

        ElemOne current = head;

        for (int i = 0; i < position - 1; i++)
        {
            current = current.getNext();
        }

        ElemOne temp = current.getNext();
        current.setNext(current.getNext().getNext());

        return (T) temp.getData();
    }

    @Override
    public int find(T dataToFind)
    {
        if (head == null)
            return -1;

        int counter = 0;
        ElemOne current = head;

        while (current.getNext() != null)
        {
            if (current.getData().equals(dataToFind))
                return counter;

            current = current.getNext();
            counter++;
        }

        if (current.getData().equals(dataToFind))
            return counter;

        return -1;
    }

    @Override
    public boolean contains(T data)
    {
        if (head == null)
            return false;

        ElemOne current = head;

        while (current.getNext() != null)
        {
            if (current.getData().equals(data))
                return true;

            current = current.getNext();
        }

        if (current.getData().equals(data))
            return true;

        return false;
    }

    @Override
    public void print()
    {
        if (head==null)
            System.out.println("Lista jest pusta !");

        ElemOne current = head;

        while (current.getNext() != null)
        {
            System.out.print(current.getData() + "=>");
            current = current.getNext();
        }
        System.out.print(current.getData());

        System.out.println();
    }

    public IList<T> join(IList<T> druga)
    {
        ListOne _pierwsza = null;
        ListOne _druga = null;

        try
        {
            _pierwsza = (ListOne) this.clone();
            _druga = (ListOne) ((ListOne<T>)druga).clone();

        } catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }

        ElemOne pTail = _pierwsza.head;
        ElemOne dHead = _druga.head;

        while (pTail.getNext() != null)
        {
            pTail = pTail.getNext();
        }

        pTail.setNext(dHead);

        return _pierwsza;
    }

    public boolean similar(IList<T> druga) throws ListException
    {
        ListOne _pierwsza = null;
        ListOne _druga = null;

        try
        {
            _pierwsza = (ListOne) this.clone();
            _druga = (ListOne) ((ListOne<T>)druga).clone();
        } catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }


        if (_pierwsza.size() != _druga.size())
            return false;

        for (int i = 0; i < size(); i++)
        {
            T value = (T) _pierwsza.get(i);
            int x = _druga.find(value);

            if (x!=-1)
                _druga.remove(x);
            else
                return false;
        }

        if (_druga.size() == 0)
            return true;

        return false;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }
}
