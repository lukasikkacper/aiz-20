package me.kacperlukasik.aiz.list;

public class ListTwo<T> implements IList<T>, Cloneable
{
    ElemTwo<T> head, tail = null;


    @Override
    public void addFirst(T newData)
    {
        if (head == null)
        {
            head = new ElemTwo(newData, null, null);
            tail = head;
        } else
        {
            ElemTwo temp = new ElemTwo(newData, head, null);
            head.setPrev(temp);
            head = temp;
        }
    }

    @Override
    public void addLast(T newData)
    {
        if (head == null)
        {
            head = new ElemTwo(newData, null, null);
            tail = head;
        } else
        {
            ElemTwo temp = new ElemTwo(newData, null, tail);
            tail.setNext(temp);
            tail = temp;
        }
    }

    @Override
    public void addAtPosition(T newData, int position) throws ListException
    {
        if (head == null && position != 0)
            throw new ListException("Nie ma takiego zakresu w liscie");

        if (position < 0 || size() < position)
            throw new ListException("Nie ma takiego zakresu w liscie");

        if (position == 0 && head == null)
        {
            addFirst(newData);
            return;
        }

        int counter = 0;
        ElemTwo current = head;

        while (current.getNext() != null)
        {
            if (counter == position)
                break;

            current = current.getNext();
            counter++;
        }

        ElemTwo temp = new ElemTwo(newData, current, current.getPrev());
        current.getPrev().setNext(temp);
        current.setPrev(temp);

    }

    @Override
    public int size()
    {
        if (head == null)
            return 0;

        int counter = 1;
        ElemTwo current = head;

        while (current.getNext() != null)
        {
            current = current.getNext();
            counter++;
        }

        return counter;
    }

    @Override
    public T get(int position) throws ListException
    {
        if (head == null && position != 0)
            throw new ListException("Nie ma takiego zakresu w liscie");

        if (position < 0 || size() <= position)
            throw new ListException("Nie ma takiego zakresu w liscie");

        ElemTwo current = head;

        for (int i = 0; i < position; i++)
        {
            current = current.getNext();
        }

        return (T) current.getData();
    }

    @Override
    public T removeFirst() throws ListException
    {
        if (head == null)
            throw new ListException("Nie ma takiego zakresu w liscie");

        ElemTwo temp = head;

        if (head.getNext() != null)
        {
            head.getNext().setPrev(null);
            head = temp.getNext();
        } else
        {
            head = null;
            tail = null;
        }


        return (T) head.getData();
    }

    @Override
    public T removeLast() throws ListException
    {
        if (head == null)
            throw new ListException("Nie ma takiego zakresu w liscie");

        ElemTwo temp = tail;
        tail.getPrev().setNext(null);
        tail = temp.getPrev();

        return (T) temp.getData();
    }

    @Override
    public T remove(int position) throws ListException
    {
        if (position < 0 || position > size())
            throw new ListException("Nie ma takiego zakresu w liscie");

        if (head == null)
            throw new ListException("Nie ma takiego zakresu w liscie");

        if (position == 0)
        {
            if (size() == 1)
            {
                ElemTwo x = head;
                head.setNext(null);
                head.setPrev(null);

                return (T) x.getData();
            }
            ElemTwo x = head;
            head = head.getNext();
            head.setPrev(null);

            return (T) x.getData();
        }

        if (position == size()-1)
        {
            ElemTwo x = tail;
            tail = tail.getPrev();
            tail.setNext(null);
            return (T) x.getData();
        }

        ElemTwo current = head.getNext();

        for (int i = 1; i < size()-1; i++)
        {
            if (i==position)
            {
                ElemTwo p = current.getPrev();
                ElemTwo n = current.getNext();

                p.setNext(n);
                n.setPrev(p);

                return (T) current.getData();
            }

            current = current.getNext();
        }

        return null;
    }

    @Override
    public int find(T dataToFind)
    {
        if (head == null)
            return -1;

        int counter = 0;
        ElemTwo current = head;

        while (current.getNext() != null)
        {
            if (current.getData().equals(dataToFind))
                return counter;

            current = current.getNext();
            counter++;
        }

        if (current.getData().equals(dataToFind))
            return counter;

        return -1;
    }

    @Override
    public boolean contains(T data)
    {
        if (head == null)
            return false;

        ElemTwo current = head;

        while (current.getNext() != null)
        {
            if (current.getData().equals(data))
                return true;

            current = current.getNext();
        }

        if (current.getData().equals(data))
            return true;

        return false;
    }

    @Override
    public void print()
    {
        if (head == null)
            System.out.println("Lista jest pusta !");

        ElemTwo current = head;

        while (current.getNext() != null)
        {
            System.out.print(current.getData() + "=>");
            current = current.getNext();
        }

        System.out.print(current.getData());

        System.out.println();
    }

    public void printReverse()
    {
        if (head == null)
            System.out.println("Lista jest pusta !");

        ElemTwo current = tail;

        while (current.getPrev() != null)
        {
            System.out.print(current.getData() + "=>");
            current = current.getPrev();
        }
        System.out.print(current.getData());

        System.out.println();
    }

    public IList<T> join(IList<T> druga)
    {
        ListTwo _pierwsza = null;
        ListTwo _druga = null;

        try
        {
            _pierwsza = (ListTwo) this.clone();
            _druga = (ListTwo) ((ListTwo<T>) druga).clone();

        } catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }

        ElemTwo pTail = _pierwsza.tail;
        ElemTwo dHead = _druga.head;

        _pierwsza.tail = _druga.tail;
        _druga.head = _pierwsza.head;
        pTail.setNext(dHead);
        dHead.setPrev(pTail);


        return _pierwsza;
    }

    public boolean similar(IList<T> druga) throws ListException
    {
        ListTwo _pierwsza = null;
        ListTwo _druga = null;

        try
        {
            _pierwsza = (ListTwo) this.clone();
            _druga = (ListTwo) ((ListTwo<T>) druga).clone();
        } catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }


        if (_pierwsza.size() != _druga.size())
            return false;

        for (int i = 0; i < size(); i++)
        {
            T value = (T) _pierwsza.get(i);
            int x = _druga.find(value);

            if (x != -1)
                _druga.remove(x+1);
            else
                return false;
        }

        if (_druga.size() == 0)
            return true;

        return false;

    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }
}
