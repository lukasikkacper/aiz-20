package me.kacperlukasik;

import me.kacperlukasik.aiz.list.ListException;
import me.kacperlukasik.aiz.list.ListOne;
import me.kacperlukasik.aiz.list.ListTwo;

public class Main
{

    public static void listaJednoKierunkowa() throws ListException
    {
        ListOne<Integer> lj1 = new ListOne<>();
        ListOne<Integer> lj2 = new ListOne<>();
        lj2.addLast(5);
        lj2.addLast(6);
        lj1.addLast(1);
        lj1.addLast(3);
        lj1.addLast(4);
        lj1.addLast(5);
        lj1.addAtPosition(2, 1);
        lj1.addFirst(0);
        lj1.print();
        System.out.println("Usunięcie pierwszego ostatniego i 2 elementu");
        lj1.removeFirst();
        lj1.removeLast();
        lj1.remove(2);
        lj1.print();
        System.out.println("Wyświetlenie elementu o indexie 1 => " + lj1.get(1));
        System.out.println("Lista zawiera element 1 => " + lj1.contains(1));
        System.out.println("Lista zawiera element 5 => " + lj1.contains(5));
        System.out.format("Pozycja elementu 1 i 5 => (%d,%d)\n", lj1.find(1), lj1.find(5));
        System.out.println("Lista po zloaczeniu dwoch list: =>");
        lj1.join(lj2).print();
    }

    public static void listaDwuKierunkowa() throws ListException
    {
        ListTwo<Integer> lj1 = new ListTwo<>();
        ListTwo<Integer> lj2 = new ListTwo<>();
        lj2.addLast(5);
        lj2.addLast(6);
        lj1.addLast(1);
        lj1.addLast(3);
        lj1.addLast(4);
        lj1.addLast(5);
        lj1.addAtPosition(2,1);
        lj1.addFirst(0);
        lj1.print();
        lj1.printReverse();
        System.out.println("Usunięcie pierwszego ostatniego i 2 elementu");
        lj1.removeFirst();
        lj1.removeLast();
        lj1.remove(2);
        lj1.print();
        lj1.printReverse();
        System.out.println("Wyświetlenie elementu o indexie 1 => "+lj1.get(1));
        System.out.println("Lista zawiera element 1 => "+lj1.contains(1));
        System.out.println("Lista zawiera element 5 => "+lj1.contains(5));
        System.out.format("Pozycja elementu 1 i 5 => (%d,%d)\n", lj1.find(1), lj1.find(5));
        System.out.println("Lista po zloaczeniu dwoch list: =>");
        ListTwo<Integer> afterJoin = (ListTwo<Integer>) lj1.join(lj2);
        afterJoin.print();
        afterJoin.printReverse();
    }

    public static void permutacje() throws ListException
    {
        ListOne<Integer> l1 = new ListOne<>();
        ListOne<Integer> l2 = new ListOne<>();
        l1.addLast(1);
        l1.addLast(2);
        l1.addLast(3);
        l2.addLast(2);
        l2.addLast(3);
        System.out.println("Lista L1:");
        l1.print();
        System.out.println("Lista L2:");
        l2.print();
        System.out.println("L1 jest permutacja L2 => " + l1.similar(l2));
        System.out.println("Dodaje 1 na poczatku list");
        l2.addFirst(1);
        System.out.println("Lista L2 po zmianach");
        l2.print();
        System.out.println("L1 jest permutacja L2 => " + l1.similar(l2));
        l1.print();
        l2.print();
    }

    public static void main(String[] args) throws ListException
    {
        System.out.println("LISTA JEDNOKIERUNKOWA");
        listaJednoKierunkowa();
        System.out.println("==================");
        System.out.println("LISTA DWUKIERUNKOWA");
        listaDwuKierunkowa();
        System.out.println("==================");
        System.out.println("PERMUTACJE");
        permutacje();
    }
}
