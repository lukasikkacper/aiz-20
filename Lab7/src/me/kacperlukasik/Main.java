package me.kacperlukasik;

public class Main {

    public static void main(String[] args) {
        Graph graph = new Graph();
        Graph graph1;
        System.out.format("Dodano wierzchołek o numerze %d\n", graph.addVertex());
        System.out.format("Dodano wierzchołek o numerze %d\n", graph.addVertex());
        System.out.format("Dodano wierzchołek o numerze %d\n", graph.addVertex());
        System.out.format("Dodano wierzchołek o numerze %d\n", graph.addVertex());
        System.out.format("Dodano wierzchołek o numerze %d\n", graph.addVertex());
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(0, 3);
        graph.addEdge(1, 3);
        graph.addEdge(1, 4);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);
        System.out.println("==Lista sąsiedztwa==");
        graph.writeList();
        System.out.println("==Macierz sasiedztwa==");
        graph.writeMatrix();
        System.out.println("=Lista sąsiedztwa dla wierzchołka 0=");
        for (int x : graph.sasiedzi(0))
        {
            System.out.print(x + " ");
        }
        System.out.println();
        System.out.println("==DFS REKURENCJA==");
        graph.dfs();
        System.out.println("==DFS BEZ REKURENCJI==");
        graph.dfsWithoutRecursion();
        graph1 = (Graph) graph.transpose();
        System.out.println("TRANZPOZYCJA");
        graph1.writeList();
        graph1.writeMatrix();
        graph1.dfsWithoutRecursion();

    }
}
