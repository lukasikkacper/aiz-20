package me.kacperlukasik;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Graph extends LGraph {

    private boolean[] visited;

    @Override
    public void writeList() {
        for (int i = 0; i < listaSasiedztwa.size(); i++) {
            System.out.print(i + ": ");

            for (int j = 0; j < listaSasiedztwa.size(); j++) {
                if (check(i, j)) {
                    System.out.format(" %d ", j);
                }
            }
            System.out.println();
        }
    }

    @Override
    public void writeMatrix() {
        System.out.print("|W|");
        for (int i = 0; i < listaSasiedztwa.size(); i++)
            System.out.format("|%d|", i);
        System.out.println();
        for (int i = 0; i < listaSasiedztwa.size(); i++) {
            System.out.format("|%d|", i);
            for (int j = 0; j < listaSasiedztwa.size(); j++)
                System.out.format("|%d|", check(i, j) ? 1 : 0);
            System.out.println();
        }
    }

    @Override
    public int addVertex() {
        listaSasiedztwa.add(new ArrayList<>());

        return iloscWierzcholkow++;
    }

    @Override
    public void addEdge(int source, int target) throws IllegalArgumentException {
        if (source < 0 || target < 0)
            throw new IllegalArgumentException("I oraz J nie może być mniejsze od zera");
        if (source > iloscWierzcholkow - 1 || target > iloscWierzcholkow - 1)
            throw new IllegalArgumentException("I oraz J nie może być większe od rozmiaru grafu");

        if (!listaSasiedztwa.get(source).contains(target))
            listaSasiedztwa.get(source).add(target);
    }

    @Override
    public List<Integer> sasiedzi(int v) throws IllegalArgumentException {
        return listaSasiedztwa.get(v);
    }

    @Override
    public boolean check(int i, int j) throws IllegalArgumentException {
        if (i < 0 || j < 0)
            throw new IllegalArgumentException("I oraz J nie może być mniejsze od zera");
        if (i > iloscWierzcholkow - 1 || j > iloscWierzcholkow - 1)
            throw new IllegalArgumentException("I oraz J nie może być większe od rozmiaru grafu");

        return listaSasiedztwa.get(i).contains(j);
    }


    //XD

    @Override
    public void dfs() {
        visited = new boolean[iloscWierzcholkow];
        for(int i = 0; i < iloscWierzcholkow; i++)
        {
            if (!visited[i])
                odwiedzaj(i);
        }
        System.out.println();
    }

    public void dfsWithoutRecursion() {
        Stack<Integer> stack = new Stack<>();
        visited = new boolean[iloscWierzcholkow];

        for(int x = 0; x < iloscWierzcholkow; x++)
        {
            if (visited[x])
                continue;

            stack.push(x);

            while (!stack.empty()) {
                int current = stack.pop();
                if (visited[current])
                    continue;

                visited[current] = true;
                System.out.format("%d -> ", current);

                List<Integer> neig = sasiedzi(current);
                for (int i = neig.size() - 1; i >= 0; i--) {
                    int u = neig.get(i);
                    if (!visited[u]) {
                        stack.push(u);
                    }
                }
            }
        }

        System.out.println();
    }

    @Override
    protected void odwiedzaj(int wierzcholek) {
        visited[wierzcholek] = true;
        System.out.format("%d -> ", wierzcholek);

        for (int current : sasiedzi(wierzcholek)) {
            if (!visited[current])
                odwiedzaj(current);
        }
    }

    @Override
    public LGraph transpose() {
        Graph graph = new Graph();

        for (int i = 0; i < iloscWierzcholkow; i++)
            graph.addVertex();

        for (int i = 0; i < iloscWierzcholkow; i++) {
            for (int j = 0; j < iloscWierzcholkow; j++) {
                if (check(i, j))
                    graph.addEdge(j, i);
            }
        }

        return graph;
    }
}
