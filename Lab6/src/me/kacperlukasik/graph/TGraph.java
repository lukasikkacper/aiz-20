package me.kacperlukasik.graph;

public class TGraph extends MGraph
{
    public TGraph(int k)
    {
        super(k);
    }

    @Override
    public int getEdgeCount()
    {
        int count = 0;

        for (int i = 0; i < getVertexCount(); i++)
        {
            for (int j = 0; j < getVertexCount(); j++)
            {
                if (isEdge(i,j))
                    count++;
            }
        }

        return count;
    }

    @Override
    public void writeMatrix()
    {
        System.out.print("| |");
        for (int i = 0; i < getVertexCount(); i++)
            System.out.format("|%d|", i);
        System.out.println();
        for (int i = 0; i < getVertexCount(); i++)
        {
            System.out.format("|%d|", i);
            for (int j = 0; j < getVertexCount(); j++)
            {
                    System.out.format("|%d|", isEdge(i,j) ? 1: 0);
            }
            System.out.println();
        }
    }
}
