package me.kacperlukasik.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BFSGraph2 extends BFSGraph
{
    public BFSGraph2(int k)
    {
        super(k);
    }

    private boolean bfsToEnd(int start, int end, int pred[], int dist[])
    {
        boolean visited[] = new boolean[getVertexCount()];
        Queue<Integer> queue = new LinkedList<>();

        for (int i = 0; i < getVertexCount(); i++)
        {
            dist[i] = Integer.MAX_VALUE;
            pred[i] = -1;
        }

        visited[start] = true;
        dist[start] = 0;
        queue.offer(start);

        while (!queue.isEmpty())
        {
            int u = queue.poll();

            for (int i = 0; i < getVertexCount(); i++)
            {
                if (isEdge(u, i) && !visited[i])
                {
                    visited[i] = true;
                    dist[i] = dist[u] + 1;
                    pred[i] = u;
                    queue.offer(i);

                    if (u == end)
                        return true;
                }
            }
        }

        return false;
    }

    private int shortestDistance(int start, int end)
    {
        int pred[] = new int[getVertexCount()];
        int dist[] = new int[getVertexCount()];

        bfsToEnd(start, end, pred, dist);

        return dist[end];
    }

    public void printShortestPath(int start, int end)
    {
        int pred[] = new int[getVertexCount()];
        int dist[] = new int[getVertexCount()];

        bfsToEnd(start, end, pred, dist);

        System.out.format("Najkrótsza droga z %d do %d wynosi: %d\n", start, end, shortestDistance(start, end));

        Stack<Integer> path = new Stack<Integer>();
        int temp = end;
        path.push(temp);

        while (pred[temp] != -1)
        {
            path.push(pred[temp]);
            temp = pred[temp];
        }

        while (!path.isEmpty())
        {
            System.out.format("%d->", path.pop());
        }
    }

    @Override
    public void bfs(int sourceVertex)
    {
        boolean visited[] = new boolean[getVertexCount()];
        int dist[] = new int[getVertexCount()];
        Queue<Integer> queue = new LinkedList<>();

        for (int i = 0; i < getVertexCount(); i++)
        {
            dist[i] = Integer.MAX_VALUE;
        }

        dist[sourceVertex] = 0;
        visited[sourceVertex] = true;
        queue.offer(sourceVertex);

        System.out.println("=BFS=");

        while (queue.size() != 0)
        {
            int x = queue.poll();
            System.out.format(" %d->", x);

            for (int i = 0; i < getVertexCount(); i++)
            {
                if (isEdge(x, i) && !visited[i])
                {
                    queue.offer(i);
                    visited[i] = true;
                    dist[i] = dist[x] + 1;
                }
            }
        }

        System.out.println();
        for (int i = 0; i < getVertexCount(); i++)
        {
            if (i != sourceVertex)
            {
                int x = dist[i];

                if (x == Integer.MAX_VALUE)
                    System.out.format("Brak drogi z %d do %d \n", sourceVertex, i);
                else
                    System.out.format("Droga z %d do %d wynosi: %d\n", sourceVertex, i, x);
            }
        }
    }
}
