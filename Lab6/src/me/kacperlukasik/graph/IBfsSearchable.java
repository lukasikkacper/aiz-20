package me.kacperlukasik.graph;

public interface IBfsSearchable {

    public void bfs(int sourceVertex);
    
}
