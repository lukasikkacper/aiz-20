package me.kacperlukasik.graph;

import java.util.LinkedList;
import java.util.Queue;

public class BFSGraph extends TGraph implements IBfsSearchable
{
    public BFSGraph(int k)
    {
        super(k);
    }

    @Override
    public void bfs(int sourceVertex)
    {
        boolean visited[] = new boolean[getVertexCount()];
        Queue<Integer> queue = new LinkedList<>();

        visited[sourceVertex] = true;
        queue.offer(sourceVertex);

        System.out.println("=BFS=");

        while (!queue.isEmpty())
        {
            int x = queue.poll();
            System.out.format(" %d->", x);

            for (int i = 0; i < getVertexCount(); i++)
            {
                if (isEdge(x,i) && !visited[i])
                {
                    queue.offer(i);
                    visited[i] = true;
                }
            }
        }
    }
}
