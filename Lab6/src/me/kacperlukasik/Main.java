package me.kacperlukasik;

import me.kacperlukasik.graph.BFSGraph;
import me.kacperlukasik.graph.BFSGraph2;
import me.kacperlukasik.graph.TGraph;

public class Main {

    public static void main(String[] args) {
        TGraph tGraph = new TGraph(9);
        tGraph.addEdge(0,1);
        tGraph.addEdge(0,2);
        tGraph.addEdge(0,3);
        tGraph.addEdge(1,4);
        tGraph.addEdge(1,5);
        tGraph.addEdge(4,1);
        tGraph.addEdge(3,6);
        tGraph.addEdge(3,7);
        tGraph.addEdge(6,2);
        tGraph.addEdge(6,8);
        tGraph.addEdge(7,8);
        tGraph.addEdge(8,5);

        System.out.println("Macierz sasiedzctwa:");
        tGraph.writeMatrix();
        System.out.format("Ilosc krawedzi: %d\n",tGraph.getEdgeCount());
        System.out.format("Ilosc wierzchołków: %d\n",tGraph.getVertexCount());

        System.out.println("============ZAD2============");
        BFSGraph bfsGraph = new BFSGraph(9);
        bfsGraph.addEdge(0,1);
        bfsGraph.addEdge(0,2);
        bfsGraph.addEdge(0,3);
        bfsGraph.addEdge(1,4);
        bfsGraph.addEdge(1,5);
        bfsGraph.addEdge(4,1);
        bfsGraph.addEdge(3,6);
        bfsGraph.addEdge(3,7);
        bfsGraph.addEdge(6,2);
        bfsGraph.addEdge(6,8);
        bfsGraph.addEdge(7,8);
        bfsGraph.addEdge(8,5);
        bfsGraph.bfs(0);

        System.out.println("\n============ZAD3============");
        BFSGraph2 bfsGraph2 = new BFSGraph2(9);
        bfsGraph2.addEdge(0,1);
        bfsGraph2.addEdge(0,2);
        bfsGraph2.addEdge(0,3);
        bfsGraph2.addEdge(1,4);
        bfsGraph2.addEdge(1,5);
        bfsGraph2.addEdge(4,1);
        bfsGraph2.addEdge(3,6);
        bfsGraph2.addEdge(3,7);
        bfsGraph2.addEdge(6,2);
        bfsGraph2.addEdge(6,8);
        bfsGraph2.addEdge(7,8);
        bfsGraph2.addEdge(8,5);
        bfsGraph2.bfs(0);

        System.out.println("\n============ZAD4============");
        BFSGraph2 bfsGraph21 = new BFSGraph2(9);
        bfsGraph21.addEdge(0,1);
        bfsGraph21.addEdge(0,2);
        bfsGraph21.addEdge(0,3);
        bfsGraph21.addEdge(1,4);
        bfsGraph21.addEdge(1,5);
        bfsGraph21.addEdge(4,1);
        bfsGraph21.addEdge(3,6);
        bfsGraph21.addEdge(3,7);
        bfsGraph21.addEdge(6,2);
        bfsGraph21.addEdge(6,8);
        bfsGraph21.addEdge(7,8);
        bfsGraph21.addEdge(8,5);
        bfsGraph21.printShortestPath(0,8);
    }
}
