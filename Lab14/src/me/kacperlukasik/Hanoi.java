package me.kacperlukasik;

public class Hanoi
{
    private static void solveRec(int n, char f, char t, char h)
    {
        if (n==1)
        {
            System.out.format("Przenieś dysk z %c do %c\n", f, t);
            return;
        }

        solveRec(n-1, f,h,t);
        solveRec(1, f,t,h);
        solveRec(n-1, h,t,f);
    }

    public static void solve(int n)
    {
        solveRec(n, 'A', 'C', 'B');
    }
}
