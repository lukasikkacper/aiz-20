package me.kacperlukasik;

public class Main {

    public static void main(String[] args) {
        System.out.println("==================");
        System.out.println("PROBLEM N HETMANÓW");
        System.out.println("==================");
	    NHetmans hetmans = new NHetmans(8);
        hetmans.solve();
        hetmans.printSolution();

        System.out.println("===============");
        System.out.println("PROBLEM SKOCZKA");
        System.out.println("===============");
        Jumper jumper = new Jumper(8);
        jumper.solve(0,0);

        System.out.println("===========");
        System.out.println("WIEŻA HANOI");
        System.out.println("===========");
        Hanoi.solve(5);

        System.out.println("======");
        System.out.println("K-GRAF");
        System.out.println("======");
        int graph[][] = {
            { 0, 1, 1, 1 },
            { 1, 0, 1, 0 },
            { 1, 1, 0, 1 },
            { 1, 0, 1, 0 },
        };
        KGraf kGraf = new KGraf(graph,2);
        kGraf.solve();
    }
}
