package me.kacperlukasik;

public class NHetmans
{
    private int n;
    private int board[][];

    public NHetmans(int n)
    {
        this.n = n;
        this.board = new int[n][n];
    }

    private boolean checkSet(int r, int c)
    {
        for (int i = 0; i < c; i++)
        {
            if (board[r][i] == 1)
                return false;
        }

        for (int i = r, j = c; i >= 0 && j >= 0; i--, j--)
        {
            if (board[i][j] == 1)
                return false;
        }

        for (int i = r, j = c; j >= 0 && i < n; i++, j--)
        {
            if (board[i][j] == 1)
                return false;
        }

        return true;
    }

    public void solve()
    {
        solveRec(0);
    }

    private boolean solveRec(int c)
    {
        if (c >= n)
            return true;

        for (int i = 0; i < n; i++)
        {
            if (checkSet(i, c))
            {
                board[i][c] = 1;

                if (solveRec(c + 1))
                    return true;

                board[i][c] = 0;
            }
        }

        return false;
    }

    public void printSolution()
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                char x = board[i][j] == 1 ? 'X' : '.';
                System.out.print(" " + x + " ");
            }

            System.out.println();
        }
    }
}
