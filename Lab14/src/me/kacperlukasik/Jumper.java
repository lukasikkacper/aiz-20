package me.kacperlukasik;

public class Jumper
{
    int xMove[] = {2, 1, -1, -2, -2, -1, 1, 2};
    int yMove[] = {1, 2, 2, 1, -1, -2, -2, -1};
    private int board[][];
    private int n;

    public Jumper(int n)
    {
        this.n = n;
        this.board = new int[n][n];

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                board[i][j] = -1;
            }
        }
    }

    public void printSolution()
    {
        for (int x = 0; x < n; x++) {
            for (int y = 0; y < n; y++)
                System.out.print(board[x][y] + " ");
            System.out.println();
        }
    }

    private boolean checkMove(int x, int y)
    {
        return (x >= 0 && x < n && y >= 0 && y < n
                && board[x][y] == -1);
    }

    public boolean solve(int x, int y)
    {
        board[x][y] = 0;

        if (!solveRec(1, x, y))
        {
            System.out.println("Brak rozwiązania");
            return false;
        } else
        {
            printSolution();
        }

        return true;
    }

    private boolean solveRec(int move, int x, int y)
    {
        if (move == n * n)
            return true;

        int k, next_x, next_y;

        if (move == n*n)
            return true;

        for (k = 0; k < 8; k++)
        {
            next_x = x + xMove[k];
            next_y = y + yMove[k];

            if (checkMove(next_x, next_y))
            {
                board[next_x][next_y] = move;

                if (solveRec(move + 1, next_x, next_y))
                    return true;
                else
                    board[next_x][next_y] = -1;
            }
        }

        return false;
    }


}
