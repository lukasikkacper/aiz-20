package me.kacperlukasik;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class KGraf
{
    private int n,k;
    private int[][] graph;
    private int[] colors;

    public KGraf(int n, int k)
    {
        this.n = n;
        this.graph = new int[n][n];
        this.colors = new int[n];
        this.k = k;

        Random rand = new Random();

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                graph[i][j] = rand.nextInt() % 2;
            }
        }
    }

    public KGraf(int[][] graph, int k)
    {
        this.graph = graph;
        this.n = graph.length;
        this.colors = new int[n];
        this.k = k;
    }

    boolean isSafe(int v, int c)
    {
        for (int i = 0; i < n; i++)
            if (graph[v][i] == 1 && c == colors[i])
                return false;
        return true;
    }

    boolean solveRec(int m, int v)
    {
        if (v == colors.length)
            return true;

        for (int c = 1; c <= m; c++)
        {
            if (isSafe(v, c))
            {
                colors[v] = c;

                if (solveRec(m, v + 1))
                    return true;

                colors[v] = 0;
            }
        }

        return false;
    }

    boolean solve()
    {
        if (!solveRec(k, 0))
        {
            System.out.println("Rozwiązanie nie istnieje");

            return false;
        }

        printSolution();

        return true;
    }

    void printSolution()
    {
        System.out.println("Znaleziono rozwiązanie:");

        for (int i = 0; i < n; i++)
            System.out.print(" " + colors[i] + " ");
        System.out.println();
    }


}
