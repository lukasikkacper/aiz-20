package me.kacperlukasik;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Zbior implements IZbior
{
    public int[] zbiorTab;

    public Zbior(int[] zbiorTab)
    {
        this.zbiorTab = zbiorTab;
    }

    public Zbior()
    {
    }

    @Override
    public void wczytaj(String filename)
    {
        File file = new File(filename);
        Scanner scanner = null;
        int tabSize = 0;
        int[] newValue = new int[0];

        try
        {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        while (scanner.hasNext())
        {
            String fileLine = scanner.nextLine();
            StringTokenizer tokenizer = new StringTokenizer(fileLine, " ");
            int n = tokenizer.countTokens();
            int[] values = new int[n];
            int i = 0;

            while (tokenizer.hasMoreElements())
            {
                values[i] = Integer.parseInt(tokenizer.nextToken());
            }

            newValue = merge(newValue, values);
        }

        zbiorTab = removeDuplicates(newValue);
    }

    @Override
    public void wypisz()
    {
        for (int i = 0; i < zbiorTab.length; i++)
            System.out.print(zbiorTab[i] + " ");
        System.out.print("\n");
    }

    @Override
    public void dodaj(int nowy)
    {
        if (czyZawiera(nowy))
            return;

        int[] x = new int[zbiorTab.length + 1];

        for (int i = 0; i < zbiorTab.length; i++)
            x[i] = zbiorTab[i];

        x[x.length - 1] = nowy;

        zbiorTab = x;
    }

    @Override
    public void usun(int element)
    {
        if (!czyZawiera(element))
            return;

        int[] x = new int[zbiorTab.length - 1];

        for (int i = 0, k = 0; i < zbiorTab.length; i++)
        {
            if (zbiorTab[i] == element)
                continue;
            x[k++] = zbiorTab[i];
        }

        zbiorTab = x;
    }

    @Override
    public boolean czyZawiera(int element)
    {
        for (int i = 0; i < zbiorTab.length; i++)
        {
            if (zbiorTab[i] == element)
                return true;
        }

        return false;
    }

    @Override
    public boolean czyRowne(IZbior drugi)
    {
        if (!(drugi instanceof Zbior))
            return false;

        Zbior x = (Zbior) drugi;

        if (this.zbiorTab.length != x.zbiorTab.length)
            return false;

        for (int i = 0; i < this.zbiorTab.length; i++)
        {
            if (!x.czyZawiera(this.zbiorTab[i]))
                return false;
        }

        for (int i = 0; i < x.zbiorTab.length; i++)
        {
            if (!this.czyZawiera(x.zbiorTab[i]))
                return false;
        }

        return true;
    }

    @Override
    public IZbior suma(IZbior drugi)
    {
        int[] c = merge(this.zbiorTab, ((Zbior) drugi).zbiorTab);

        return new Zbior(removeDuplicates(c));
    }

    @Override
    public IZbior roznica(IZbior drugi)
    {
        Zbior x = new Zbior(zbiorTab);

        for (int i = 0; i < ((Zbior) drugi).zbiorTab.length; i++)
        {
            x.usun(((Zbior) drugi).zbiorTab[i]);
        }

        return x;
    }

    @Override
    public IZbior przeciecie(IZbior drugi)
    {
        Zbior b = (Zbior) drugi;
        Zbior x = new Zbior(new int[0]);

        for(int i = 0; i < this.zbiorTab.length; i++)
        {
            if (b.czyZawiera(this.zbiorTab[i]))
                x.dodaj(this.zbiorTab[i]);
        }

        for(int i = 0; i < b.zbiorTab.length; i++)
        {
            if (czyZawiera(b.zbiorTab[i]))
                x.dodaj(b.zbiorTab[i]);
        }

        x.zbiorTab = removeDuplicates(x.zbiorTab);

        return x;
    }

    @Override
    public IZbior roznicaSymetryczna(IZbior drugi)
    {
        Zbior b = (Zbior) drugi;
        Zbior x = (Zbior) suma(b);
        Zbior d = (Zbior) this.przeciecie(b);

        for (int i = 0; i < d.zbiorTab.length; i++)
        {
            x.usun(d.zbiorTab[i]);
        }

        return x;
    }

    private int[] merge(int[] a, int[] b)
    {
        int[] c = new int[a.length + b.length];

        for (int i = 0; i < c.length; i++)
        {
            if (i < c.length - b.length)
                c[i] = a[i];
            else
                c[i] = b[i - a.length];
        }

        return c;
    }

    private int[] removeDuplicates(int[] arr)
    {
        int end = arr.length;
        for (int i = 0; i < end; i++)
        {
            for (int j = i + 1; j < end; j++)
            {
                if (arr[i] == arr[j])
                {
                    arr[j] = arr[end - 1];
                    end--;
                    j--;
                }
            }
        }

        int[] whitelist = new int[end];
        System.arraycopy(arr, 0, whitelist, 0, end);

        return whitelist;
    }
}
