package me.kacperlukasik;

import org.w3c.dom.Node;

import java.util.TreeMap;

public class TreeNode extends BinaryTreeNode
{

    /**
     * Konstruktor.
     *
     * @param dane
     */
    public TreeNode(int dane)
    {
        super(dane);
    }

    public TreeNode(int dane, TreeNode lewy, TreeNode prawy)
    {
        super(dane);
        this.lewy = lewy;
        this.prawy = prawy;
    }

    @Override
    public void print(int poziom)
    {
        if (lewy != null)
            lewy.print(poziom + 1);

        System.out.println(contTabByN(poziom) + dane);

        if (prawy != null)
            prawy.print(poziom + 1);
    }


    public void printOrder()
    {
        printOrderRec(getSize() - 1);
    }

    public void printOrderRec(int poziom)
    {
        if (prawy != null)
            prawy.printOrderRec(poziom - 1);

        System.out.println(contTabByN(poziom) + dane);

        if (lewy != null)
            lewy.printOrderRec(poziom - 1);
    }

    @Override
    public boolean searchBSTRec(int szukany)
    {
        if (szukany == dane)
            return true;

        if (this.lewy != null)
        {
            if (this.lewy.searchBSTRec(szukany))
                return true;
        }

        if (this.prawy != null)
        {
            if (this.prawy.searchBSTRec(szukany))
                return true;
        }

        return false;
    }

    @Override
    public void addBSTRec(int nowy)
    {
        if (nowy == dane)
            return;

        if (dane > nowy)
        {
            if (lewy != null)
                lewy.addBSTRec(nowy);
            else
                lewy = new TreeNode(nowy);
        } else if (dane < nowy)
        {
            if (prawy != null)
                prawy.addBSTRec(nowy);
            else
                prawy = new TreeNode(nowy);
        }
    }

    @Override
    public Pair<BinaryTreeNode, BinaryTreeNode> searchBST(int szukany)
    {
        BinaryTreeNode prev = null;
        BinaryTreeNode current = this;

        while (current != null && current.dane != szukany)
        {
            prev = current;

            if (szukany < current.dane)
            {
                current = current.lewy;
            } else if (szukany > current.dane)
            {
                current = current.prawy;
            }
        }

        return new Pair<BinaryTreeNode, BinaryTreeNode>(current, prev);
    }

    public void insertBST(int nowy)
    {
        Pair<BinaryTreeNode, BinaryTreeNode> searchPair = searchBST(nowy);

        if (searchPair.first == null)
        {
            TreeNode insertedNode = new TreeNode(nowy);

            if (nowy < searchPair.second.dane)
                searchPair.second.lewy = insertedNode;
            else
                searchPair.second.prawy = insertedNode;
        }
    }

    public void delete(int removeValue)
    {
        deleteRec(removeValue, this);
    }

    private TreeNode deleteRec(int removeValue, TreeNode current)
    {
        if (current == null)
        {
            return null;
        }

        if (removeValue == current.dane)
        {
            //brak rodzicow
            if (current.lewy == null && current.prawy == null)
            {
                return null;
            }

            //2 przypadek jedno z rodzicow
            if (current.prawy == null)
            {
                return (TreeNode) current.lewy;
            }

            if (current.lewy == null)
            {
                return (TreeNode) current.prawy;
            }

            // dwoch dzieci
            int smallest = findSmallest((TreeNode) current.prawy);
            current.dane = smallest;
            current.prawy = deleteRec(smallest, (TreeNode) current.prawy);

            return current;
        }

        if (removeValue < current.dane)
        {
            current.lewy = deleteRec(removeValue, (TreeNode) current.lewy);
            return current;
        }

        current.prawy = deleteRec(removeValue, (TreeNode) current.prawy);

        return current;
    }

    public int findSmallest(TreeNode node)
    {
        return node.lewy == null ? node.dane : findSmallest((TreeNode) node.lewy);
    }

    public static String contTabByN(int n)
    {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < n; i++)
        {
            stringBuilder.append("\t");
        }

        return stringBuilder.toString();
    }

    public int getSize()
    {
        return getSizeRecursive(this);
    }

    private int getSizeRecursive(TreeNode current)
    {
        if (current == null)
            return 0;

        int leftSize = getSizeRecursive((TreeNode) current.lewy);
        int rightSize = getSizeRecursive((TreeNode) current.prawy);

        return Integer.max(leftSize, rightSize) + 1;

    }
}
