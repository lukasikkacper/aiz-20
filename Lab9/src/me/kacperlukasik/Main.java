package me.kacperlukasik;

import com.sun.source.tree.Tree;

public class Main {

    public static void main(String[] args) {
        /*TreeNode tempNode = new TreeNode(8);
        tempNode.insertBST(3);
        tempNode.insertBST(13);
        tempNode.insertBST(2);
        tempNode.insertBST(4);
        tempNode.insertBST(12);
        tempNode.insertBST(14);

        TreeNode treeNode = new TreeNode(8);
        treeNode.addBSTRec(3);
        treeNode.addBSTRec(13);
        treeNode.addBSTRec(2);
        treeNode.addBSTRec(4);
        treeNode.addBSTRec(12);
        treeNode.addBSTRec(14);


        System.out.println("================================");
        System.out.println("Graf po dodawaniu rekurencyjnym:");
        System.out.println("================================");
        treeNode.print(0);
        System.out.println("============OBRÓCONY============");
        treeNode.printOrder();
        System.out.println("==============================");
        System.out.println("Graf po dodawaniu iteracyjnym:");
        System.out.println("==============================");
        tempNode.print(0);
        System.out.format("Graf zawiera 12? %b\n", tempNode.searchBSTRec(12));
        System.out.println("===========================");
        System.out.println("Graf po usunięciu węzła 12:");
        System.out.println("===========================");
        tempNode.delete(8);
        tempNode.delete(12);
        tempNode.print(0);
        System.out.format("Graf zawiera 12? %b\n", tempNode.searchBSTRec(12));*/

        TreeNode wysokosc = new TreeNode(8);
        wysokosc.addBSTRec(3);
        wysokosc.addBSTRec(5);
        wysokosc.addBSTRec(2);
        wysokosc.addBSTRec(1);
        wysokosc.addBSTRec(4);
        wysokosc.addBSTRec(0);
        wysokosc.print(0);
        System.out.println("===============");
        wysokosc.printOrder();
        System.out.format("Wysokość: %d \n", wysokosc.getSize());
    }
}
