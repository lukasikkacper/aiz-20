package me.kacperlukasik;

import java.time.Duration;
import java.time.Instant;

public class Main {

    public static void main(String[] args) {
    	Instant start,end;
    	int x;

		MySort insert = new MySort();
		insert.wypelnij(10, 100);
		insert.wypisz();
		MySort selection = insert.copy();
		MySort bubble = insert.copy();

		System.out.println("INSERTION SORT:");
		start = Instant.now();
		insert.sortuj(true, Sort.SortMethod.INSERTION);
		end = Instant.now();
		System.out.format("Czas wykonania: %d ms\n", Duration.between(start,end).toMillis());

		System.out.println("SELECTION SORT:");
		start = Instant.now();
		selection.sortuj(true, Sort.SortMethod.SELECTION);
		end = Instant.now();
		System.out.format("Czas wykonania: %d ms\n", Duration.between(start,end).toMillis());

		System.out.println("BUBBLE SORT:");
		start = Instant.now();
		bubble.sortuj(true, Sort.SortMethod.BUBBLE);
		end = Instant.now();
		System.out.format("Czas wykonania: %d ms\n", Duration.between(start,end).toMillis());
	}
}
