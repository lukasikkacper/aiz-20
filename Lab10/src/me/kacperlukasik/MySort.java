package me.kacperlukasik;

import java.util.Arrays;
import java.util.Random;

import static java.util.Arrays.copyOf;

public class MySort extends Sort
{
    @Override
    protected int losuj(int w_max)
    {
        int randomValue;
        Random random = new Random();
        randomValue = random.nextInt(w_max) + 1;

        return randomValue;
    }

    @Override
    protected void selectionsort(boolean rosnaco)
    {
        int compCount = 0;

        for (int i = 0; i < ile; i++)
        {
            int minIndex = i;

            for (int j = i + 1; j < ile; j++)
            {
                if ((rosnaco && tab[j] < tab[minIndex])
                        || (!rosnaco && tab[j] > tab[minIndex]))
                    minIndex = j;

                compCount++;
            }

            int temp = tab[minIndex];
            tab[minIndex] = tab[i];
            tab[i] = temp;
        }

        System.out.format("Ilosc porownan: %d\n", compCount);
    }

    @Override
    protected void insertsort(boolean rosnaco)
    {
        int compCount = 0;
        for (int i = 1; i < ile; i++)
        {
            int value = tab[i];
            int j = i - 1;

            while (j >= 0 && ((rosnaco && tab[j] > value)
                    || (!rosnaco && tab[j] < value)))
            {
                tab[j + 1] = tab[j];
                j = j - 1;
                compCount++;
            }

            tab[j + 1] = value;
        }

        System.out.format("Ilosc porownan: %d\n", compCount);
    }

    @Override
    protected void bubblesort(boolean rosnaco)
    {
        int compCount = 0;
        for (int i = 0; i < ile - 1; i++)
        {
            for (int j = 0; j < ile - i - 1; j++)
            {
                if ((rosnaco && tab[j] > tab[j + 1])
                        || (!rosnaco && tab[j] > tab[j + 1]))
                {
                    int temp = tab[j];
                    tab[j] = tab[j + 1];
                    tab[j + 1] = temp;
                }
                compCount++;
            }
        }

        System.out.format("Ilosc porownan: %d\n", compCount);
    }

    @Override
    public void wypelnij(int n, int wartosc_maksymalna)
    {
        tab = new int[n];
        ile = n;

        for (int i = 0; i < n; i++)
        {
            tab[i] = losuj(wartosc_maksymalna);
        }
    }

    @Override
    public void sortuj(boolean rosnaco, SortMethod metoda)
    {
        switch (metoda)
        {
            case BUBBLE -> bubblesort(rosnaco);
            case INSERTION -> insertsort(rosnaco);
            case SELECTION -> selectionsort(rosnaco);
        }
    }

    public MySort copy()
    {
        MySort newMySort = new MySort();
        newMySort.tab = new int[ile];
        System.arraycopy(this.tab, 0, newMySort.tab, 0, ile);
        newMySort.ile = this.ile;

        return newMySort;
    }

    @Override
    public void wypisz()
    {
        for (int i = 0; i < ile; i++)
        {
            System.out.format("%d ", tab[i]);
        }

        System.out.println();
    }
}
