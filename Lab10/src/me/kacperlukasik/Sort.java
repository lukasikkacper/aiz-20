package me.kacperlukasik;

public abstract class Sort {

    public enum SortMethod {

        SELECTION, INSERTION, BUBBLE;
    }

    int[] tab; // tablica liczb do sortowania
    int ile;      // ile element�w jest w tablicy

    protected abstract int losuj(int w_max); // zwraca losowa liczbe z przedzialu 0..w_max

    protected abstract void selectionsort(boolean rosnaco);

    protected abstract void insertsort(boolean rosnaco);
	
	protected abstract void bubblesort(boolean rosnaco);

    public abstract void wypelnij(int n, int wartosc_maksymalna);// wypelnia tablice n losowymi liczbami z przedzialu 0..w_max

    public abstract void sortuj(boolean rosnaco, SortMethod metoda); // wywoluje odpowiedni algorytm sortowania 

    public abstract void wypisz(); // wypisuje zawartosc tablicy tab
}
