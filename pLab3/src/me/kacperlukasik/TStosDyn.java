package me.kacperlukasik;

public class TStosDyn extends TStos
{
    public TStosDyn()
    {
        super();
    }

    public TStosDyn(int size)
    {
        super(size);
    }

    public TStosDyn(TStos stos)
    {
        super(stos);
    }

    @Override
    public void push(int i) throws Exception
    {
        if (super.w == super.stos.length)
        {
            int[] x = new int[super.stos.length*2];
            System.arraycopy(super.stos, 0, x, 0, super.stos.length);
            super.stos = x;
        }

        super.push(i);
    }
}
