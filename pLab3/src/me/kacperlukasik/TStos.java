package me.kacperlukasik;

public class TStos implements IStos
{
    protected int[] stos;
    protected int w;

    public TStos()
    {
        this.stos = new int[10];
        this.w = 0;
    }

    public TStos(int size)
    {
        this.stos = new int[size];
        this.w = 0;
    }

    public TStos(TStos stos)
    {
        this.stos = stos.stos;
        this.w = stos.w;
    }


    @Override
    public void push(int i) throws Exception
    {
        if (w == stos.length)
            throw new Exception("Stos pełny !");

        stos[w] = i;
        w++;
    }

    @Override
    public int pop() throws Exception
    {
        if (w == 0)
            throw new Exception("Stos pusty !");

        int old = w - 1;
        w--;

        return stos[old];
    }

    @Override
    public int peek() throws Exception
    {
        if (w==0)
            throw new Exception("Stos pusty !");

        return stos[w-1];
    }

    @Override
    public boolean isEmpty()
    {
        return w == 0;
    }

    @Override
    public void print() throws Exception
    {
        if (w==0)
            throw new Exception("Stos pusty !");

        for (int i = w - 1; i >= 0; i--)
            System.out.format("| %d |", stos[i]);

        System.out.print("\n");
    }

    @Override
    public void clear()
    {
        stos = new int[stos.length];
        w = 0;
    }
}
