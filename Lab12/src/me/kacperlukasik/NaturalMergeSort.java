package me.kacperlukasik;

import java.util.Random;

public class NaturalMergeSort
{
    private static Comparable[] aux;

    public static void sort(Comparable[] a)
    {
        aux = new Comparable[a.length];
        sort(a, 0, a.length - 1);
    }

    private static boolean isSorted(Comparable[] a)
    {
        for (int i = 1; i < a.length; i++)
        {
            if (a[i - 1].compareTo(a[i]) > 0)
                return false;
        }

        return true;
    }

    private static void sort(Comparable[] a, int lo, int hi)
    {
        int i = lo;
        int j = 0;

        while (true)
        {
            i = 0;
            while (i < a.length)
            {
                if (i == a.length - 1)
                {
                    break;
                } else if (a[i].compareTo(a[i + 1]) > 0)
                {
                    break;
                }
                i++;
            }

            j = i + 1;

            while (j < a.length)
            {
                if (j == a.length - 1)
                {
                    break;
                } else if (a[j].compareTo(a[j + 1]) > 0)
                {
                    break;
                }
                j++;
            }
            merge(a, lo, i, j);
            lo = 0;

            if (isSorted(a))
            {
                break;
            }
        }
    }

    private static void merge(Comparable[] a, int lo, int mid, int hi)
    {
        int i = lo;
        int j = mid + 1;

        for (int k = lo; k <= hi; k++)
        {
            aux[k] = a[k];
        }

        for (int k = lo; k <= hi; k++)
        {
            if (i > mid)
            {
                a[k] = aux[j++];
            } else if (j > hi)
            {
                a[k] = aux[i++];
            } else if (aux[i].compareTo(aux[j]) > 0)
            {
                a[k] = aux[j++];
            } else
            {
                a[k] = aux[i++];
            }
        }
    }

    public static void print(Comparable[] a)
    {
        for (int i = 0; i < a.length; i++)
        {
            System.out.print(a[i] + " ");
        }
    }

    public static Comparable[] randomize(int n)
    {
        Comparable[] temp = new Comparable[n];

        for (int i = 0; i < n; i++)
            temp[i] = random(0, 200);

        return temp;
    }

    private static int random(int min, int max)
    {
        Random random = new Random();

        return random.ints(min, max)
                .findFirst()
                .getAsInt();
    }
}
