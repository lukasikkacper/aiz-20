package me.kacperlukasik;

public class Pattern
{
    public static void searchPattern(String y, String x)
    {
        int counter = 0;
        int n = y.length();
        int m = x.length();

        int i = 0;

        System.out.print("Indexy: ");

        while (i<=n-m)
        {
            int j = 0;

            while (j<m && x.charAt(j) == y.charAt(i+j))
            {
                j++;
            }

            if(j==m)
            {
                counter++;
                System.out.print(i+" ");
            }

            i++;
        }

        System.out.println("\nIlość wystąpień: " + counter);
    }
}
