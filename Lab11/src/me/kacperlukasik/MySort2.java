package me.kacperlukasik;

import java.util.Arrays;
import java.util.Random;

public class MySort2 extends Sort2
{
    @Override
    protected int losuj(int w_max)
    {
        int randomValue;
        Random random = new Random();
        randomValue = random.nextInt(w_max) + 1;

        return randomValue;
    }

    @Override
    public void wypelnij(int n, int wartosc_maksymalna)
    {
        tab = new int[n];
        ileT = n;

        for (int i = 0; i < n; i++)
        {
            tab[i] = losuj(wartosc_maksymalna);
        }
    }

    public MySort2 copy()
    {
        MySort2 newMySort = new MySort2();
        newMySort.tab = new int[ileT];
        System.arraycopy(this.tab, 0, newMySort.tab, 0, ileT);
        newMySort.ileT = this.ileT;

        return newMySort;
    }

    @Override
    public void wypisz()
    {
        for (int i = 0; i < ileT; i++)
        {
            System.out.format("%d ", tab[i]);
        }

        System.out.println();
    }

    @Override
    public void wypiszDrzewo()
    {
        int space = 5;
        int level = 1;

        writeSpace(space);
        System.out.print(tab[0]);
        space = writeSpace(space);
        System.out.print("\n");

        for (int i = 1; i < ileT; i++)
        {
            if (2 * i + 1 >= ileT) break;
            if (level % 2 == 0)
            {
                writeSpace(space);
                if (2 * i + 1 < ileT)
                    System.out.print(tab[2 * i + 1]);
                writeSpace(space);
                writeSpace(space);
                if (2 * i + 2 < ileT)
                    System.out.print(tab[2 * i + 2]);
                writeSpace(space);
                level++;
            } else
            {
                writeSpace(space);
                if (2 * i + 1 < ileT)
                    System.out.print(tab[2 * i + 1]);
                writeSpace(space);
                writeSpace(space);
                if (2 * i + 2 < ileT)
                    System.out.print(tab[2 * i + 2]);
                space = writeSpace(space);
                level++;
                System.out.print("\n");
            }
        }
        System.out.print("\n");
    }

    private int writeSpace(int space)
    {
        for (int i = 0; i < Math.pow(2, space); i++)
            System.out.print(" ");

        return space - 1;
    }

    @Override
    protected void downHeap(int i)
    {
        int v = tab[i];
        while (i <= (ileT - 1) / 2)
        {
            int j = 2 * i;
            if (j < ileT - 1)
                if (tab[j] < tab[j + 1])
                    j++;
            if (v >= tab[j])
                break;
            tab[i] = tab[j];
            i = j;
        }
        tab[i] = v;
    }

    @Override
    protected void construct()
    {
        for (int i = ileT / 2 - 1; i >= 0; i--)
            createHeap(ileT, i);
    }

    private void createHeap(int n, int i)
    {
        int largest = i;
        int l = 2 * i + 1;
        int r = 2 * i + 2;

        if (l < n && tab[l] > tab[largest])
            largest = l;

        if (r < n && tab[r] > tab[largest])
            largest = r;

        if (largest != i)
        {
            int swap = tab[i];
            tab[i] = tab[largest];
            tab[largest] = swap;

            createHeap(n, largest);
        }
    }

    @Override
    protected int deleteMax()
    {
        int deleted = tab[0];
        tab[0] = tab[ileT - 1];
        ileT--;
        createHeap(ileT, 0);

        return deleted;
    }

    @Override
    protected void heapSort()
    {
        construct();

        for (int i = ileT - 1; i > 0; i--)
        {

            int temp = tab[0];
            tab[0] = tab[i];
            tab[i] = temp;

            createHeap(i, 0);
        }
    }

    private int partition(int[] arr, int min, int max)
    {
        int pivot = arr[max];
        int i = min - 1;

        for (int j = min; j < max; j++)
        {
            if (arr[j] < pivot)
            {
                i++;

                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        int temp = arr[i + 1];
        arr[i + 1] = arr[max];
        arr[max] = temp;

        return i + 1;
    }

    @Override
    protected void quicksort()
    {
        int min = 0;
        int max = ileT - 1;

        sortQuick(min, max);
    }

    private void sortQuick(int min, int max)
    {
        if (min < max)
        {
            int partitionIndex = partition(tab, min, max);

            sortQuick(min, partitionIndex - 1);
            sortQuick(partitionIndex + 1, max);
        }
    }

    @Override
    protected void countsort()
    {
        int max = Arrays.stream(tab).max().getAsInt();
        int min = Arrays.stream(tab).min().getAsInt();
        int range = max - min + 1;

        int count[] = new int[range];
        int output[] = new int[tab.length];


        for (int i = 0; i < tab.length; i++)
        {
            count[tab[i] - min]++;
        }

        for (int i = 1; i < count.length; i++)
        {
            count[i] += count[i - 1];
        }

        for (int i = tab.length - 1; i >= 0; i--)
        {
            output[count[tab[i] - min] - 1] = tab[i];
            count[tab[i] - min]--;
        }

        for (int i = 0; i < tab.length; i++)
        {
            tab[i] = output[i];
        }
    }
}
